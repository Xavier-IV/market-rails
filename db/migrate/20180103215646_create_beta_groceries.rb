class CreateBetaGroceries < ActiveRecord::Migration[5.1]
  def change
    create_table "beta_groceries", primary_key: "grocery_id", force: :cascade do |t|
      t.string "grocery_name", limit: 20, null: false
      t.date "grocery_expiry_date", null: false
      t.string "grocery_description", limit: 50, null: false
      t.string "grocery_price", limit: 10, null: false
      t.string "grocery_image", limit: 1000, null: false
      t.integer "fk1_type_id", precision: 38
      t.integer "fk2_brand_id", precision: 38
      t.integer "fk3_store_id", precision: 38
    end

    add_foreign_key "beta_groceries", "beta_grocery_brands", column: "fk2_brand_id", primary_key: "brand_id", name: "sys_c0035057"
    add_foreign_key "beta_groceries", "beta_grocery_stores", column: "fk3_store_id", primary_key: "store_id", name: "sys_c0035058"
    add_foreign_key "beta_groceries", "beta_grocery_types", column: "fk1_type_id", primary_key: "type_id", name: "sys_c0035056"
  end
end
