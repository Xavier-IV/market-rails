# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180103215646) do

  create_table "beta_customer_orders", primary_key: "cust_order_id", force: :cascade do |t|
    t.date "order_date", null: false
    t.integer "order_total", precision: 38, null: false
    t.decimal "total_payment", precision: 8, scale: 2, null: false
    t.integer "id", precision: 38
    t.integer "order_id", precision: 38
  end

  create_table "beta_groceries", primary_key: "grocery_id", force: :cascade do |t|
    t.string "grocery_name", limit: 20, null: false
    t.date "grocery_expiry_date", null: false
    t.string "grocery_description", limit: 50, null: false
    t.string "grocery_price", limit: 10, null: false
    t.string "grocery_image", limit: 1000, null: false
    t.integer "fk1_type_id", precision: 38
    t.integer "fk2_brand_id", precision: 38
    t.integer "fk3_store_id", precision: 38
  end

  create_table "beta_grocery_brands", primary_key: "brand_id", force: :cascade do |t|
    t.string "brand_name", limit: 20, null: false
  end

  create_table "beta_grocery_customers", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: 6
    t.datetime "remember_created_at", precision: 6
    t.integer "sign_in_count", precision: 38, default: 0, null: false
    t.datetime "current_sign_in_at", precision: 6
    t.datetime "last_sign_in_at", precision: 6
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "i_beta_grocery_customers_email", unique: true
    t.index ["reset_password_token"], name: "i_bet_gro_cus_res_pas_tok", unique: true
  end

  create_table "beta_grocery_deliveries", primary_key: "delivery_id", force: :cascade do |t|
    t.string "delivery_methode", limit: 20, null: false
    t.integer "delivery_address", precision: 38
    t.integer "fk1_cust_order_id", precision: 38
    t.integer "fk2_staff_id", precision: 38
  end

  create_table "beta_grocery_orders", primary_key: "order_id", force: :cascade do |t|
    t.integer "quantity", precision: 38, null: false
    t.decimal "total_quantity", precision: 8, scale: 2, null: false
    t.integer "fk1_grocery_id", precision: 38
  end

  create_table "beta_grocery_payments", primary_key: "payment_id", force: :cascade do |t|
    t.integer "payment_type", precision: 38, null: false
    t.string "payment_methode", limit: 20, null: false
    t.integer "payment_price", precision: 38, null: false
    t.date "payment_date", null: false
    t.integer "cust_order_id", precision: 38
    t.integer "staff_id", precision: 38
  end

  create_table "beta_grocery_ratings", primary_key: "rating_id", force: :cascade do |t|
    t.integer "rating_value", precision: 38, null: false
    t.integer "fk1_grocery_id", precision: 38
    t.integer "fk2_id", precision: 38
  end

  create_table "beta_grocery_staff_roles", primary_key: "role_id", force: :cascade do |t|
    t.string "role_name", limit: 15, null: false
  end

  create_table "beta_grocery_staffs", primary_key: "staff_id", force: :cascade do |t|
    t.string "staff_name", limit: 20, null: false
    t.string "staff_email", limit: 20, null: false
    t.string "staff_tel_no", limit: 15, null: false
    t.integer "role_id", precision: 38
  end

  create_table "beta_grocery_stores", primary_key: "store_id", force: :cascade do |t|
    t.string "store_address", limit: 50, null: false
  end

  create_table "beta_grocery_types", primary_key: "type_id", force: :cascade do |t|
    t.string "type_name", limit: 20, null: false
  end

  add_foreign_key "beta_customer_orders", "beta_grocery_customers", column: "id", name: "sys_c0035087"
  add_foreign_key "beta_customer_orders", "beta_grocery_orders", column: "order_id", primary_key: "order_id", name: "sys_c0035088"
  add_foreign_key "beta_groceries", "beta_grocery_brands", column: "fk2_brand_id", primary_key: "brand_id", name: "sys_c0035057"
  add_foreign_key "beta_groceries", "beta_grocery_stores", column: "fk3_store_id", primary_key: "store_id", name: "sys_c0035058"
  add_foreign_key "beta_groceries", "beta_grocery_types", column: "fk1_type_id", primary_key: "type_id", name: "sys_c0035056"
  add_foreign_key "beta_grocery_deliveries", "beta_customer_orders", column: "fk1_cust_order_id", primary_key: "cust_order_id", name: "sys_c0035092"
  add_foreign_key "beta_grocery_deliveries", "beta_grocery_staffs", column: "fk2_staff_id", primary_key: "staff_id", name: "sys_c0035093"
  add_foreign_key "beta_grocery_orders", "beta_groceries", column: "fk1_grocery_id", primary_key: "grocery_id", name: "sys_c0035081"
  add_foreign_key "beta_grocery_payments", "beta_customer_orders", column: "cust_order_id", primary_key: "cust_order_id", name: "sys_c0035105"
  add_foreign_key "beta_grocery_payments", "beta_grocery_staffs", column: "staff_id", primary_key: "staff_id", name: "sys_c0035106"
  add_foreign_key "beta_grocery_ratings", "beta_groceries", column: "fk1_grocery_id", primary_key: "grocery_id", name: "sys_c0035097"
  add_foreign_key "beta_grocery_ratings", "beta_grocery_customers", column: "fk2_id", name: "sys_c0035098"
  add_foreign_key "beta_grocery_staffs", "beta_grocery_staff_roles", column: "role_id", primary_key: "role_id", name: "sys_c0035067"
end
