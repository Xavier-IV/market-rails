class SearchController < ApplicationController
  def index
  end

  def result
    @result = BetaGrocery.where('lower(grocery_name) LIKE :search', search: "%#{params[:q]}%")

  end
end
