class HomepageController < ApplicationController
  def index

    @data = BetaGroceryCustomer.all

    @all_groceries = BetaGrocery.all

  end

  def show
    @item = params[:id]

    @show = BetaGrocery.find(@item)


  end

  def create
    id = params[:show][:id]
    total = params[:quantity]
    (session[:items] ||= []) << ["item" => id, "total" => total]
    redirect_to homepage_index_path
  end

end
