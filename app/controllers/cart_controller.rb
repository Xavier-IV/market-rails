class CartController < ApplicationController
  def index
    @list_items = session[:items]
  end

  def show
  end

  def create
  end

  def register
    session[:items] = nil
    redirect_to root_path
  end

  def new

  end
end
