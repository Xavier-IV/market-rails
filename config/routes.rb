Rails.application.routes.draw do

  get 'cart/index'

  get 'cart/show'

  get 'cart/create'

  get 'cart/new'

  get 'cart/register'

  get 'search/index'

  get 'search/result'

  devise_for :beta_grocery_customers
  resource :homepage
  resource :cart
  get 'homepage/index'
  get 'homepage/show'
  post 'homepage/create'
  root to: 'homepage#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
